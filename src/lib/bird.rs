use circle_boundary::{calculate, Boundary};
use lib::bounds::Bounds;
use lib::offset::Offset;
use lib::position::Position;

#[derive(Debug, Clone)]
enum Group {
    Follow,
    Flee,
}

#[derive(Debug, Clone)]
pub struct Bird {
    position: Position,
    velocity: Offset,
    acceleration: Offset,
    group: Group,
}

impl Bird {
    pub fn within_bounds(bounds: &Bounds) -> Bird {
        Bird {
            group: Group::Follow,
            position: Position::random_within_bounds(bounds),
            velocity: Offset::random(),
            acceleration: Offset::random(),
        }
    }

    pub fn adjust_path(&self, birds: &Vec<Bird>) -> Bird {
        let local_range = calculate(self.position.x, self.position.y, 108);
        let local_birds = birds.into_iter().filter(|other_bird| {
            !self.ref_eq(other_bird) && self.other_bird_is_close(&local_range, other_bird)
        });
        let mut transported_bird = self.clone();
        for _bird in local_birds {
            transported_bird.position.x += 1;
            transported_bird.position.y += 1;
            transported_bird.group = Group::Flee;
        }
        transported_bird
    }

    fn other_bird_is_close(&self, close_range: &Vec<Boundary>, other_bird: &Bird) -> bool {
        let mut in_range = false;
        for Boundary { x, y } in close_range {
            if (other_bird.position.x == *x)
                && (y.start <= other_bird.position.y && other_bird.position.y <= y.end)
            {
                in_range |= true;
            }
        }
        in_range
    }

    fn ref_eq(&self, other_bird: &Bird) -> bool {
        let me = self as *const _;
        let you = other_bird as *const _;
        let result = me == you;
        result
    }
}
