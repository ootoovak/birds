use rand::prelude::{thread_rng, Rng};

#[derive(Debug, Clone)]
pub struct Offset {
    x: i32,
    y: i32,
}

impl Offset {
    pub fn random() -> Offset {
        let mut rng = thread_rng();

        Offset {
            x: rng.gen_range(-10, 10),
            y: rng.gen_range(-10, 10),
        }
    }
}
