use lib::bounds::Bounds;
use rand::prelude::{thread_rng, Rng};

#[derive(Debug, PartialEq, Clone)]
pub struct Position {
    pub x: i32,
    pub y: i32,
}

impl Position {
    pub fn random_within_bounds(bounds: &Bounds) -> Position {
        let mut rng = thread_rng();

        Position {
            x: rng.gen_range(bounds.min.x, bounds.max.x),
            y: rng.gen_range(bounds.min.y, bounds.max.y),
        }
    }
}
