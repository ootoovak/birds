use lib::bird::Bird;
use lib::bounds::Bounds;
use lib::position::Position;

#[derive(Debug)]
pub struct World {
    birds: Vec<Bird>,
    bounds: Bounds,
}

impl World {
    pub fn new(entities: i32) -> World {
        let mut birds = Vec::new();

        let bounds = Bounds {
            min: Position { x: 0, y: 0 },
            max: Position { x: 192, y: 108 },
        };

        for _index in 0..entities {
            birds.push(Bird::within_bounds(&bounds))
        }

        World { birds, bounds }
    }

    pub fn run_one_cycle(&mut self) {
        println!("========== Cycle ==========");
        self.birds = self.birds
            .clone()
            .into_iter()
            .map(|bird| bird.adjust_path(&self.birds))
            .collect();
    }
}
