use lib::position::Position;

#[derive(Debug)]
pub struct Bounds {
    pub min: Position,
    pub max: Position,
}
