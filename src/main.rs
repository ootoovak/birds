extern crate rand;
extern crate circle_boundary;

mod lib;
use lib::World;

fn main() {
    let mut world = World::new(2);

    println!("{:#?}", world);

    world.run_one_cycle();
    println!("{:#?}", world);
}
